#include <iostream> // allows input and output
#include <string> // allows the use of strings

void Name(); // initializes the method
void Age(); // initializes the method
void Alien(); // initializes the method

void main()
{
	std::cout << "Welcome to my exceptional exception tester! \n"; // outputs text
	std::cout << "There's one exception to using this tester you have to accept that errors happen... \n"; // outputs text
	std::cout << "First things first... \n"; // outputs text

	Name(); // calls a method
	Age(); // calls a method

	std::cout << " Now that that's out of the way... \n"; // outputs text

	Alien(); // calls a method

	std::cout << "Thank you for your time! \n"; // outputs text
	std::cout << "I will be sure to tell the hive... I mean your leader of your decision! \n"; // outputs text
}

void Name()
{
	std::string name; // holds a string
	std::cout << "What is your name? \n"; // outputs text
	std::getline(std::cin, name); // gets user input
	std::cout << "Okay " << name << " lets get started with the exceptions! \n"; // outputs text
	system("Pause"); // pauses
}

void Age()
{
	int birthYear; // holds an int
	int age; // holds an int
	
	std::string tempnum; // holds a string
	
	do
	{
		try
		{
			system("CLS"); // clears the console
			std::cout << "How old are you? \n"; // outputs text
			std::cin >> tempnum; // gets user input
			
			age = std::stoi(tempnum); // makes the input an int

			std::cout << "What year were you born? \n"; // outputs text
			std::cin >> tempnum; // gets user input

			birthYear = std::stoi(tempnum); // makes the input an int
			
			if ((age + birthYear) > 2020)
			{
				throw 1; // forces an error
			}

			if (birthYear < 1900)
			{
				throw 2.0f; // forces an error
			}
		}
		catch (int tooHigh)
		{
			if (tooHigh == 1)
			{
				std::cout << "It seems that you entered some data incorrectly please try again... \n"; // outputs text
				age = NULL; // clears the variable
			}

			std::cout << "Error #" << tooHigh << " has occurred! \n"; // outputs text
		}
		catch (float tooLow)
		{
			if (tooLow == 2.0f)
			{
				std::cout << "It would appear that you think you are older than you are please try again... \n"; // outputs text
				age = NULL; // clears the variable
			}

			std::cout << "Error #" << tooLow << " has occurred! \n"; // outputs text
		}
		catch (...)
		{
			std::cout << "It seems that you entered something other than a number please try again... \n"; // outputs text
			age = NULL; // clears the variable

			std::cout << "Error #3 has occurred! \n"; // outputs text
		}

		
		system("Pause"); // pauses
	}
	while (age == NULL);
	
}

void Alien()
{
	int choice; // holds an int
	
	std::string tempnum; // holds a string
	do
	{
		try
		{
			system("CLS"); // clears the console
			std::cout << "An alien species has invaded what do you do? \n"; // outputs text
			
			std::cout << "1. Take them to your leader! \n"; // outputs text
			std::cout << "2. Whip out your uno reverse card and invade the aliens! \n"; // outputs text
			std::cout << "3. Just accept it aliens are a better alternative anyway... \n"; // outputs text
			std::cin >> tempnum; // gets user input

			choice = std::stoi(tempnum); // makes the input an int

			if (choice != 1 && choice != 2 && choice != 3)
			{
				throw 4; // forces an error
			}
			
		}
		catch (int wrongNumber)
		{
			if (wrongNumber == 4)
			{
				std::cout << "Please pick either 1, 2, or 3 \n"; // outputs text
				choice = NULL; // clears the variable
			}

			std::cout << "Error #" << wrongNumber << " has occurred! \n"; // outputs text
		}
		catch (...)
		{
			std::cout << "It seems that you entered something other than a number please try again... \n"; // outputs text
			choice = NULL; // clears the variable
		}
		
		system("Pause"); // pauses
	}
	while (choice == NULL);
}